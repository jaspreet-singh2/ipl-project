const prob3 = require("../src/server/3-Extra-runs-conceded-per-team").prob3;

const sampleData =
    
        [
            {"extra_runs": "2", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "0", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "4", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "1", "bowling_team": "Royal Challengers Bangalore"},
            {"extra_runs": "3", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "5", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "0", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "7", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "6", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "2", "bowling_team": "Kings XI Punjab"},
            {"extra_runs": "0", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "4", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "3", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "1", "bowling_team": "Royal Challengers Bangalore"},
            {"extra_runs": "7", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "5", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "2", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "0", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "6", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "3", "bowling_team": "Kings XI Punjab"},
            {"extra_runs": "0", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "4", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "3", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "1", "bowling_team": "Royal Challengers Bangalore"},
            {"extra_runs": "7", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "5", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "2", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "0", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "6", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "3", "bowling_team": "Kings XI Punjab"},
            {"extra_runs": "0", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "4", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "3", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "1", "bowling_team": "Royal Challengers Bangalore"},
            {"extra_runs": "7", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "5", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "2", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "0", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "6", "bowling_team": "Rajasthan Royals"},
            {"extra_runs": "3", "bowling_team": "Kings XI Punjab"},
            {"extra_runs": "0", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "4", "bowling_team": "Chennai Super Kings"},
            {"extra_runs": "3", "bowling_team": "Delhi Daredevils"},
            {"extra_runs": "1", "bowling_team": "Royal Challengers Bangalore"},
            {"extra_runs": "7", "bowling_team": "Kolkata Knight Riders"},
            {"extra_runs": "5", "bowling_team": "Rajasthan Royals"}
          ]
          
      



describe('Extra-runs-conceded-per-team' , () =>{
    
    it('test case 2',() => {
        expect(prob3(sampleData)).toStrictEqual({
  'Kolkata Knight Riders': 33,
  'Chennai Super Kings': 23,
  'Delhi Daredevils': 22,
  'Royal Challengers Bangalore': 5,
  'Rajasthan Royals': 49,
  'Kings XI Punjab': 11
})
    })
})

//console.log(prob3(sampleData))