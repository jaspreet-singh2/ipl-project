function compare_economy(a, b) {
  return a[Object.keys(a)[0]] - b[Object.keys(b)[0]];
}

function topEconomicalBowlers(deliveriesof2015) {
  bowlers = {};
  for (let ball of deliveriesof2015) {
    if (bowlers[ball["bowler"]] === undefined) {
      bowlers[ball["bowler"]] = [1, parseInt(ball["total_runs"])];
    } else {
      bowlers[ball["bowler"]][0]++;
      bowlers[ball["bowler"]][1] += parseInt(ball["total_runs"]);
    }
  }

  let ans = [];
  for (let bowler in bowlers) {
    let econmmy = {};
    econmmy[bowler] = parseFloat(
      (bowlers[bowler][1] / (bowlers[bowler][0] / 6)).toFixed(2)
    );
    ans.push(econmmy);
  }
  ans.sort(compare_economy);

  //console.log(ans);
  return ans;
}

exports.prob4 = topEconomicalBowlers;
