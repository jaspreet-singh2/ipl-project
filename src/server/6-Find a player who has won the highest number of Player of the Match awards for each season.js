function highestPlayerPerYear(matchesPerYear) {
  tempResult = {};
  result = {};
  for (let year in matchesPerYear) {
    for (let match in matchesPerYear[year]) {
      if (
        tempResult[matchesPerYear[year][match]["player_of_match"]] === undefined
      ) {
        tempResult[matchesPerYear[year][match]["player_of_match"]] = 1;
      } else {
        tempResult[matchesPerYear[year][match]["player_of_match"]]++;
      }
    }
    // console.log(years[j]);
    maxPlayerName = "";
    maxPlayerOfTheMatch = 0;
    for (let key in tempResult) {
      if (tempResult[key] > maxPlayerOfTheMatch) {
        maxPlayerName = key;
        maxPlayerOfTheMatch = tempResult[key];
      }
    }
    // console.log(ob);
    result[year] = maxPlayerName;
    maxPlayerName = "";
    maxPlayerOfTheMatch = 0;
    //console.log(ob);
    tempResult = {};
  }
  //console.log(result);
  return result;
}
exports.prob6 = highestPlayerPerYear;
