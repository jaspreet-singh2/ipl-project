
const prob1 = require("../src/server/1-matches-per-year").prob1;

const sampleData =[
    {
        "season" : 2011
    },
    {
        "season" : 2010
    },
    {
        "season" : 2011
    },
    {
        "season" : 2012
    },
    {
        "season" : 2011
    },
    {
        "season" : 2007
    },
    {
        "season" : 2010
    }

]


describe('Number of matches played per year for all the years in IPL' , () =>{
    
    it('test case 2',() => {
        expect(prob1(sampleData)).toStrictEqual({ 
            '2007': 1,
            '2010': 2,
            '2011': 3,
            '2012': 1
        })
    })
})

//console.log(matchesPerYear(sampleData))