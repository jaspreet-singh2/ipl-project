const { readFileSync, writeFileSync } = require("fs");
const prob1 = require("./src/server/1-matches-per-year").prob1;
const prob2 = require("./src/server/2-matches-won-per-team-per-year").prob2;
const prob3 = require("./src/server/3-Extra-runs-conceded-per-team").prob3;
const prob4 =
  require("./src/server/4-Top 10 economical bowlers in the year 2015").prob4;
const prob5 =
  require("./src/server/5-number-of-times_each_team_won_the_toss_and_also_won_the_match").prob5;
const prob6 =
  require("./src/server/6-Find a player who has won the highest number of Player of the Match awards for each season").prob6;
const prob7 =
  require("./src/server/7-Find the strike rate of a batsman for each season").prob7;
const prob8 =
  require("./src/server/8-Find the highest number of times one player has been dismissed by another player").prob8;
const prob9 =
  require("./src/server/9-Find the bowler with the best economy in super overs").prob9;
require("./src/public/output/deliveries.JSON");
deliveries = JSON.parse(readFileSync("./src/public/output/deliveries.JSON"));
matchesPerYear = JSON.parse(
  readFileSync("./src/public/output/matchesPerYear.JSON")
);
matches = JSON.parse(readFileSync("./src/public/output/matches.JSON"));
yearWiseDeliveries = JSON.parse(
  readFileSync("./src/public/output/yearWiseDeliveries.JSON")
);

prob7v = prob7(yearWiseDeliveries);
prob9v = prob9(deliveries);
prob8v = prob8(deliveries);
prob5v = prob5(matches);
prob4v = prob4(yearWiseDeliveries[2015]);
prob3v = prob3(yearWiseDeliveries[2016]);
prob2v = prob2(matchesPerYear);
prob6v = prob6(matchesPerYear);
prob1v = prob1(matches);

writeFileSync(
  "./src/public/output/1-matches-per-year.JSON",
  JSON.stringify(prob1v),
  "utf8"
);
writeFileSync(
  "./src/public/output/2-matches-won-per-team-per-year.JSON",
  JSON.stringify(prob2v),
  "utf8"
);
writeFileSync(
  "./src/public/output/3-Extra-runs-conceded-per-team.JSON",
  JSON.stringify(prob3v),
  "utf8"
);
writeFileSync(
  "./src/public/output/4-Top 10 economical bowlers in the year 2015.JSON",
  JSON.stringify(prob4v),
  "utf8"
);
writeFileSync(
  "./src/public/output/5-number-of-times_each_team_won_the_toss_and_also_won_the_match.JSON",
  JSON.stringify(prob5v),
  "utf8"
);
writeFileSync(
  "./src/public/output/6-Find a player who has won the highest number of Player of the Match awards for each season.JSON",
  JSON.stringify(prob6v),
  "utf8"
);
writeFileSync(
  "./src/public/output/7-Find the strike rate of a batsman for each season.JSON",
  JSON.stringify(prob7v),
  "utf8"
);
writeFileSync(
  "./src/public/output/8-Find the highest number of times one player has been dismissed by another player.JSON",
  JSON.stringify(prob8v),
  "utf8"
);
writeFileSync(
  "./src/public/output/9-Find the bowler with the best economy in super overs.JSON",
  JSON.stringify(prob9v),
  "utf8"
);
