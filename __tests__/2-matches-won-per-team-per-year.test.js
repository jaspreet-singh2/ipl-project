const prob2 = require("../src/server/2-matches-won-per-team-per-year").prob2;
const sampleData = 
  {
    2008: [
      { winner: "Kolkata Knight Riders" },
      { winner: "Chennai Super Kings" },
      { winner: "Delhi Daredevils" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Kings XI Punjab" },
    ],
    2009: [
      { winner: "Mumbai Indians" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Delhi Daredevils" },
      { winner: "Deccan Chargers" },
      { winner: "Chennai Super Kings" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Deccan Chargers" },
      { winner: "Delhi Daredevils" },
      { winner: "Rajasthan Royals" },
      { winner: "Kings XI Punjab" },
    ],
    2010: [
      { winner: "Chennai Super Kings" },
      { winner: "Mumbai Indians" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Rajasthan Royals" },
      { winner: "Chennai Super Kings" },
      { winner: "Kings XI Punjab" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
    ],
    2011: [
      { winner: "Chennai Super Kings" },
      { winner: "Mumbai Indians" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Rajasthan Royals" },
      { winner: "Chennai Super Kings" },
      { winner: "Kings XI Punjab" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
    ],
    2012: [
      { winner: "Kolkata Knight Riders" },
      { winner: "Chennai Super Kings" },
      { winner: "Mumbai Indians" },
      { winner: "Delhi Daredevils" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Rajasthan Royals" },
      { winner: "Mumbai Indians" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Kings XI Punjab" },
    ],
    2013: [
      { winner: "Mumbai Indians" },
      { winner: "Chennai Super Kings" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Rajasthan Royals" },
      { winner: "Mumbai Indians" },
      { winner: "Kings XI Punjab" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
    ],
    2014: [
      { winner: "Kolkata Knight Riders" },
      { winner: "Kings XI Punjab" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Sunrisers Hyderabad" },
      { winner: "Chennai Super Kings" },
      { winner: "Kings XI Punjab" },
      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
    ],
    2015: [
      { winner: "Mumbai Indians" },
      { winner: "Chennai Super Kings" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Rajasthan Royals" },
      { winner: "Mumbai Indians" },
      { winner: "Kings XI Punjab" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Chennai Super Kings" },
      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
    ],
    2016: [
      { winner: "Sunrisers Hyderabad" },
      { winner: "Royal Challengers Bangalore" },
      { winner: "Delhi Daredevils" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Sunrisers Hyderabad" },
      { winner: "Gujarat Lions" },
      { winner: "Delhi Daredevils" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Rajasthan Royals" },
      { winner: "Kings XI Punjab" },
    ],
    2017: [
      { winner: "Mumbai Indians" },
      { winner: "Rising Pune Supergiant" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Sunrisers Hyderabad" },
      { winner: "Mumbai Indians" },
      { winner: "Kings XI Punjab" },
      { winner: "Kolkata Knight Riders" },
      { winner: "Mumbai Indians" },

      { winner: "Rajasthan Royals" },
      { winner: "Delhi Daredevils" },
    ],
  }


describe('3-Extra-runs-conceded-per-team' , () =>{

  it('test case 2',() => {
      expect(prob2(sampleData)).toStrictEqual([
  {
    '2008': {
      'Kolkata Knight Riders': 2,
      'Chennai Super Kings': 2,
      'Delhi Daredevils': 2,
      'Royal Challengers Bangalore': 1,
      'Rajasthan Royals': 2,
      'Kings XI Punjab': 1
    }
  },
  {
    '2009': {
      'Mumbai Indians': 1,
      'Royal Challengers Bangalore': 1,
      'Delhi Daredevils': 2,
      'Deccan Chargers': 2,
      'Chennai Super Kings': 1,
      'Kolkata Knight Riders': 1,
      'Rajasthan Royals': 1,
      'Kings XI Punjab': 1
    }
  },
  {
    '2010': {
      'Chennai Super Kings': 3,
      'Mumbai Indians': 1,
      'Royal Challengers Bangalore': 2,
      'Rajasthan Royals': 2,
      'Kings XI Punjab': 1,
      'Delhi Daredevils': 1
    }
  },
  {
    '2011': {
      'Chennai Super Kings': 3,
      'Mumbai Indians': 1,
      'Royal Challengers Bangalore': 2,
      'Rajasthan Royals': 2,
      'Kings XI Punjab': 1,
      'Delhi Daredevils': 1
    }
  },
  {
    '2012': {
      'Kolkata Knight Riders': 2,
      'Chennai Super Kings': 2,
      'Mumbai Indians': 2,
      'Delhi Daredevils': 1,
      'Rajasthan Royals': 2,
      'Kings XI Punjab': 1
    }
  },
  {
    '2013': {
      'Mumbai Indians': 2,
      'Chennai Super Kings': 2,
      'Royal Challengers Bangalore': 2,
      'Rajasthan Royals': 2,
      'Kings XI Punjab': 1,
      'Delhi Daredevils': 1
    }
  },
  {
    '2014': {
      'Kolkata Knight Riders': 2,
      'Kings XI Punjab': 2,
      'Chennai Super Kings': 2,
      'Rajasthan Royals': 2,
      'Sunrisers Hyderabad': 1,
      'Delhi Daredevils': 1
    }
  },
  {
    '2015': {
      'Mumbai Indians': 2,
      'Chennai Super Kings': 2,
      'Royal Challengers Bangalore': 2,
      'Rajasthan Royals': 2,
      'Kings XI Punjab': 1,
      'Delhi Daredevils': 1
    }
  },
  {
    '2016': {
      'Sunrisers Hyderabad': 2,
      'Royal Challengers Bangalore': 1,
      'Delhi Daredevils': 2,
      'Kolkata Knight Riders': 2,
      'Gujarat Lions': 1,
      'Rajasthan Royals': 1,
      'Kings XI Punjab': 1
    }
  },
  {
    '2017': {
      'Mumbai Indians': 3,
      'Rising Pune Supergiant': 1,
      'Kolkata Knight Riders': 2,
      'Sunrisers Hyderabad': 1,
      'Kings XI Punjab': 1,
      'Rajasthan Royals': 1,
      'Delhi Daredevils': 1
    }
  }
])
  })
 })

//console.log(prob2(sampleData));
