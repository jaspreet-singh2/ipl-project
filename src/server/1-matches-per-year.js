function matches_per_year(matches) {
  matches_per_year_ob = {};
  for (let i = 0; i < matches.length; i++) {
    if (matches_per_year_ob[matches[i]["season"]] === undefined) {
      matches_per_year_ob[matches[i]["season"]] = 1;
    } else {
      matches_per_year_ob[matches[i]["season"]]++;
    }
  }
  //console.log(ob)
  return matches_per_year_ob;
}

exports.prob1 = matches_per_year;
