const prob4 =
  require("../src/server/4-Top 10 economical bowlers in the year 2015").prob4;

const sampleData = [
  {
    total_runs: "3",
    bowler: "KC Cariappa",
  },
  {
    total_runs: "1",
    bowler: "RP Singh",
  },
  {
    total_runs: "4",
    bowler: "MR Marsh",
  },
  {
    total_runs: "2",
    bowler: "I Sharma",
  },
  {
    total_runs: "0",
    bowler: "MJ McClenaghan",
  },
  {
    total_runs: "7",
    bowler: "MJ McClenaghan",
  },
  {
    total_runs: "5",
    bowler: "JJ Bumrah",
  },
  {
    total_runs: "6",
    bowler: "JJ Bumrah",
  },
  {
    total_runs: "0",
    bowler: "Harbhajan Singh",
  },
  {
    total_runs: "7",
    bowler: "Harbhajan Singh",
  },
  {
    total_runs: "2",
    bowler: "HH Pandya",
  },
  {
    total_runs: "4",
    bowler: "HH Pandya",
  },
  {
    total_runs: "7",
    bowler: "UT Yadav",
  },
  {
    total_runs: "1",
    bowler: "C Munro",
  },
  {
    total_runs: "6",
    bowler: "PP Chawla",
  },
  {
    total_runs: "3",
    bowler: "PJ Sangwan",
  },
  {
    total_runs: "4",
    bowler: "S Ladda",
  },
  {
    total_runs: "0",
    bowler: "RA Jadeja",
  },
  {
    total_runs: "5",
    bowler: "RA Jadeja",
  },
  {
    total_runs: "2",
    bowler: "Sandeep Sharma",
  },
  {
    total_runs: "7",
    bowler: "MP Stoinis",
  },
  {
    total_runs: "3",
    bowler: "A Nehra",
  },
  {
    total_runs: "6",
    bowler: "B Kumar",
  },
  {
    total_runs: "0",
    bowler: "MC Henriques",
  },
  {
    total_runs: "7",
    bowler: "KV Sharma",
  },
  {
    total_runs: "1",
    bowler: "KV Sharma",
  },
  {
    total_runs: "5",
    bowler: "KV Sharma",
  },
  {
    total_runs: "0",
    bowler: "KV Sharma",
  },
  {
    total_runs: "7",
    bowler: "MC Henriques",
  },
  {
    total_runs: "6",
    bowler: "A Ashish Reddy",
  },
  {
    total_runs: "0",
    bowler: "A Ashish Reddy",
  },
  {
    total_runs: "3",
    bowler: "KV Sharma",
  },
  {
    total_runs: "7",
    bowler: "KV Sharma",
  },
  {
    total_runs: "4",
    bowler: "KV Sharma",
  },
  {
    total_runs: "6",
    bowler: "B Kumar",
  },
  {
    total_runs: "2",
    bowler: "Mustafizur Rahman",
  },
  {
    total_runs: "0",
    bowler: "AF Milne",
  },
  {
    total_runs: "1",
    bowler: "HV Patel",
  },
  {
    total_runs: "6",
    bowler: "Parvez Rasool",
  },
  {
    total_runs: "3",
    bowler: "YS Chahal",
  },
  {
    total_runs: "4",
    bowler: "YS Chahal",
  },
  {
    total_runs: "7",
    bowler: "YS Chahal",
  },
  {
    total_runs: "5",
    bowler: "YS Chahal",
  },
  {
    total_runs: "0",
    bowler: "Parvez Rasool",
  },
  {
    total_runs: "7",
    bowler: "AF Milne",
  },
  {
    total_runs: "6",
    bowler: "HH Pandya",
  },
  {
    total_runs: "0",
    bowler: "Harbhajan Singh",
  },
  {
    total_runs: "7",
    bowler: "J Suchith",
  },
  {
    total_runs: "3",
    bowler: "J Suchith",
  },
  {
    total_runs: "1",
    bowler: "TG Southee",
  },
  {
    total_runs: "7",
    bowler: "TG Southee",
  },
  {
    total_runs: "4",
    bowler: "Harbhajan Singh",
  },
];

describe("4-Top 10 economical bowlers in the year 2015.test", () => {
  it("test case 2", () => {
    expect(prob4(sampleData)).toStrictEqual([
      { "RP Singh": 6 },
      { "C Munro": 6 },
      { "HV Patel": 6 },
      { "I Sharma": 12 },
      { "Sandeep Sharma": 12 },
      { "Mustafizur Rahman": 12 },
      { "RA Jadeja": 15 },
      { "Harbhajan Singh": 16.5 },
      { "KC Cariappa": 18 },
      { "PJ Sangwan": 18 },
      { "A Nehra": 18 },
      { "A Ashish Reddy": 18 },
      { "Parvez Rasool": 18 },
      { "MJ McClenaghan": 21 },
      { "MC Henriques": 21 },
      { "AF Milne": 21 },
      { "KV Sharma": 23.14 },
      { "MR Marsh": 24 },
      { "HH Pandya": 24 },
      { "S Ladda": 24 },
      { "TG Southee": 24 },
      { "YS Chahal": 28.5 },
      { "J Suchith": 30 },
      { "JJ Bumrah": 33 },
      { "PP Chawla": 36 },
      { "B Kumar": 36 },
      { "UT Yadav": 42 },
      { "MP Stoinis": 42 },
    ]);
  });
});

//console.log(prob4(sampleData));
