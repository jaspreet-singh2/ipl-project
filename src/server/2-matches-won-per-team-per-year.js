function matchWonPerTeamPerYear(matchesPerYear) {
  let wonPerYear = {};
  let result = [];
  for (let year in matchesPerYear) {
    for (let match of matchesPerYear[year]) {
      if (wonPerYear[match["winner"]] === undefined) {
        wonPerYear[match["winner"]] = 1;
      } else {
        wonPerYear[match["winner"]]++;
      }
    }
    wonThisYear = {};
    wonThisYear[year] = wonPerYear;
    result.push(wonThisYear);
    wonPerYear = {};
  }
  //console.log(list);
  return result;
}
exports.prob2 = matchWonPerTeamPerYear;
