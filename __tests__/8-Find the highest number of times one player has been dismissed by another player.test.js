const prob8= require("../src/server/8-Find the highest number of times one player has been dismissed by another player").prob8;
const sampleData =[
       
        
        {
          "player_dismissed": "KM Jadhav",
          "bowler": "VR Aaron"
        },
        {
          "player_dismissed": "KM Jadhav",
          "bowler": "VR Aaron"
        },
        {
          "player_dismissed": "M Vohra",
          "bowler": "YS Chahal"
        },
        {
          "player_dismissed": "AR Patel",
          "bowler": "YS Chahal"
        },
        
        {
          "player_dismissed": "AM Rahane",
          "bowler": "Z Khan"
        },
        {
          "player_dismissed": "AM Rahane",
          "bowler": "Z Khan"
        },
        {
          "player_dismissed": "RA Tripathi",
          "bowler": "CH Morris"
        },
        {
          "player_dismissed": "F du Plessis",
          "bowler": "S Nadeem"
        },
        {
          "player_dismissed": "BA Stokes",
          "bowler": "PJ Cummins"
        },
        {
          "player_dismissed": "MS Dhoni",
          "bowler": "A Mishra"
        },
        {
          "player_dismissed": "MS Dhoni",
          "bowler": "A Mishra"
        },
        
      ]



describe('Find the highest number of times one player has been dismissed by another player' , () =>{
    
    it('test case 2',() => {
        expect(prob8(sampleData)).toStrictEqual(
          [
            { 'VR Aaron': { 'KM Jadhav': 2 } },
            { 'YS Chahal': { 'M Vohra': 1 } },
            { 'Z Khan': { 'AM Rahane': 2 } },
            { 'CH Morris': { 'RA Tripathi': 1 } },
            { 'S Nadeem': { 'F du Plessis': 1 } },
            { 'PJ Cummins': { 'BA Stokes': 1 } },
            { 'A Mishra': { 'MS Dhoni': 2 } }
          ]
           )
    })
})

//console.log(prob8(sampleData))
  