years = [];
const fs = require("fs");

function yearsF(starting_season) {
  if (starting_season < 2018) {
    years.push(starting_season);
    yearsF(++starting_season);
  }
}
yearsF(2008);

function filter(matches, deliveries) {
  let matchesPerYear = {};
  let yearMatch;
  let yearWiseDeliveries = {};
  for (year of years) {
    yearMatch = matches.filter((x) => {
      return x["season"] == year;
    });
    matchesPerYear[year] = yearMatch;
    year_delveries = [];

    for (match of yearMatch) {
      year_delveries = year_delveries.concat(
        deliveries.filter((x) => {
          return match["id"] == x["match_id"];
        })
      );
    }
    yearWiseDeliveries[year] = year_delveries;

    year_delveries = [];
  }
  writetojson(matches,matchesPerYear,yearWiseDeliveries,deliveries)

}

function writetojson(matches,matchesPerYear,yearWiseDeliveries,deliveries)
{
    fs.writeFileSync(
        "../public/output/matches.JSON",
        JSON.stringify(matches),
        "utf8"
      );
      fs.writeFileSync(
        "../public/output/yearWiseDeliveries.JSON",
        JSON.stringify(yearWiseDeliveries),
        "utf8"
      );
      fs.writeFileSync(
        "../public/output/matchesPerYear.JSON",
        JSON.stringify(matchesPerYear),
        "utf8"
      );
      fs.writeFileSync(
        "../public/output/deliveries.JSON",
        JSON.stringify(deliveries),
        "utf8"
      );
}
exports.filter=filter