function numberOfTimesTeamWonTossAndMatch(matches) {
  result = {};
  for (let match of matches) {
    if (match["winner"] == match["toss_winner"]) {
      result[match["winner"]] =
        result[match["winner"]] === undefined ? 1 : result[match["winner"]] + 1;
    }
  }
  //console.log(result);
  return result;
}
exports.prob5 = numberOfTimesTeamWonTossAndMatch;
