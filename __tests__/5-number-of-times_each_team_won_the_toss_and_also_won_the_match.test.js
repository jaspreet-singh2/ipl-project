
const prob5= require("../src/server/5-number-of-times_each_team_won_the_toss_and_also_won_the_match").prob5;


  

const sampleData =[
    {
        "winner": "Royal Challengers Bangalore",
        "toss_winner": "Royal Challengers Bangalore"
      },
      {
        "winner": "Sunrisers Hyderabad",
        "toss_winner": "Pune Warriors"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Chennai Super Kings"
      },
      {
        "winner": "Sunrisers Hyderabad",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Royal Challengers Bangalore",
        "toss_winner": "Royal Challengers Bangalore"
      },
      {
        "winner": "Delhi Daredevils",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Kings XI Punjab",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Royal Challengers Bangalore",
        "toss_winner": "Pune Warriors"
      },
      {
        "winner": "Kings XI Punjab",
        "toss_winner": "Delhi Daredevils"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Sunrisers Hyderabad"
      },
      {
        "winner": "Kolkata Knight Riders",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Sunrisers Hyderabad"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Delhi Daredevils",
        "toss_winner": "Pune Warriors"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Chennai Super Kings"
      },
      {
        "winner": "Sunrisers Hyderabad",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Delhi Daredevils",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Chennai Super Kings"
      },
      {
        "winner": "Royal Challengers Bangalore",
        "toss_winner": "Royal Challengers Bangalore"
      },
      {
        "winner": "Kolkata Knight Riders",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Sunrisers Hyderabad",
        "toss_winner": "Delhi Daredevils"
      },
      {
        "winner": "Kings XI Punjab",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Pune Warriors"
      },
      {
        "winner": "Royal Challengers Bangalore",
        "toss_winner": "Sunrisers Hyderabad"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Delhi Daredevils"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Sunrisers Hyderabad"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Kolkata Knight Riders",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Royal Challengers Bangalore",
        "toss_winner": "Delhi Daredevils"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Pune Warriors"
      },
      {
        "winner": "Sunrisers Hyderabad",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Kolkata Knight Riders",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Rajasthan Royals",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Kings XI Punjab",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Sunrisers Hyderabad"
      },
      {
        "winner": "Pune Warriors",
        "toss_winner": "Kolkata Knight Riders"
      },
      {
        "winner": "Chennai Super Kings",
        "toss_winner": "Chennai Super Kings"
      },
      {
        "winner": "Mumbai Indians",
        "toss_winner": "Rajasthan Royals"
      },
      {
        "winner": "Kings XI Punjab",
        "toss_winner": "Kings XI Punjab"
      },
      {
        "winner": "Sunrisers Hyderabad",
        "toss_winner": "Sunrisers Hyderabad"
      },
      {
        "winner": "Kings XI Punjab",
        "toss_winner": "Mumbai Indians"
      },
      {
        "winner": "Pune Warriors",
        "toss_winner": "Pune Warriors"
      }
]


describe('5-number-of-times_each_team_won_the_toss_and_also_won_the_match' , () =>{
   
    it('test case 2',() => {
        expect(prob5(sampleData)).toStrictEqual({
  'Royal Challengers Bangalore': 3,
  'Rajasthan Royals': 4,
  'Chennai Super Kings': 4,
  'Kings XI Punjab': 4,
  'Mumbai Indians': 4,
  'Kolkata Knight Riders': 2,
  'Sunrisers Hyderabad': 1,
  'Pune Warriors': 1
})
    })
})

//console.log(prob5(sampleData))