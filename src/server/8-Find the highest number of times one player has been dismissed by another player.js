function highesPlayerDissmised(deliveries) {
  dismisedPerYear = {};
  let ans = [];
  for (let ball of deliveries) {
    if (ball["player_dismissed"] != "") {
      if (dismisedPerYear[ball["bowler"]] === undefined) {
        dismisedPerYear[ball["bowler"]] = {};
      }
      if (
        dismisedPerYear[ball["bowler"]][ball["player_dismissed"]] === undefined
      ) {
        dismisedPerYear[ball["bowler"]][ball["player_dismissed"]] = 1;
      } else dismisedPerYear[ball["bowler"]][ball["player_dismissed"]]++;
    }
  }
  //console.log( dismisedPerYear)

  for (let bowler in dismisedPerYear) {
    maxbowler = "";
    noOfTimesBatsmanDismised = 0;
    maxbatsman = "";
    for (dismisedBatsman in dismisedPerYear[bowler]) {
      if (noOfTimesBatsmanDismised < dismisedPerYear[bowler][dismisedBatsman]) {
        noOfTimesBatsmanDismised = dismisedPerYear[bowler][dismisedBatsman];
        maxbowler = bowler;
        maxbatsman = dismisedBatsman;
      }
    }
    let boller = {};
    boller[maxbatsman] = noOfTimesBatsmanDismised;
    let MaxNoOfTimesBatsmanDismised = {};
    MaxNoOfTimesBatsmanDismised[maxbowler] = boller;
    ans.push(MaxNoOfTimesBatsmanDismised);
    maxbowler = "";
    noOfTimesBatsmanDismised = 0;
  }

  // console.log(ans)
  return ans;
}

exports.prob8 = highesPlayerDissmised;
