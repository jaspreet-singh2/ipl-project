
const prob7= require("../src/server/7-Find the strike rate of a batsman for each season").prob7;
const sampleData =
    {
        "2008": [
          {"batsman": "BB McCullum", "total_runs": 4, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 6, "extra_runs": 3},
          {"batsman": "SC Ganguly", "total_runs": 1, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 3, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 2, "extra_runs": 0}
        ],
        "2009": [
          {"batsman": "BB McCullum", "total_runs": 2, "extra_runs": 3},
          {"batsman": "BB McCullum", "total_runs": 5, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 6, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 4, "extra_runs": 3},
          {"batsman": "BB McCullum", "total_runs": 1, "extra_runs": 3}
        ],
        "2010": [
          {"batsman": "SC Ganguly", "total_runs": 4, "extra_runs": 3},
          {"batsman": "RT Ponting", "total_runs": 6, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 7, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 6, "extra_runs": 3},
          {"batsman": "RT Ponting", "total_runs": 1, "extra_runs": 3}
        ],
        "2011": [
          {"batsman": "BB McCullum", "total_runs": 5, "extra_runs": 3},
          {"batsman": "SC Ganguly", "total_runs": 2, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 7, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 1, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 4, "extra_runs": 0}
        ],
        "2012": [
          {"batsman": "BB McCullum", "total_runs": 7, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 5, "extra_runs": 3},
          {"batsman": "RT Ponting", "total_runs": 4, "extra_runs": 3},
          {"batsman": "SC Ganguly", "total_runs": 1, "extra_runs": 3},
          {"batsman": "BB McCullum", "total_runs": 2, "extra_runs": 3}
        ],
        "2013": [
          {"batsman": "RT Ponting", "total_runs": 1, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 6, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 4, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 5, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 6, "extra_runs": 0}
        ],
        "2014": [
          {"batsman": "BB McCullum", "total_runs": 3, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 4, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 7, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 6, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 2, "extra_runs": 3}
        ],
        "2015": [
          {"batsman": "SC Ganguly", "total_runs": 3, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 7, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 5, "extra_runs": 3},
          {"batsman": "RT Ponting", "total_runs": 1, "extra_runs": 3},
          {"batsman": "SC Ganguly", "total_runs": 2, "extra_runs": 0}
        ],
        "2016": [
          {"batsman": "SC Ganguly", "total_runs": 6, "extra_runs": 3},
          {"batsman": "RT Ponting", "total_runs": 5, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 1, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 4, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 3, "extra_runs": 0}
        ],
        "2017": [
          {"batsman": "RT Ponting", "total_runs": 7, "extra_runs": 0},
          {"batsman": "BB McCullum", "total_runs": 2, "extra_runs": 0},
          {"batsman": "SC Ganguly", "total_runs": 6, "extra_runs": 0},
          {"batsman": "RT Ponting", "total_runs": 4, "extra_runs": 3},
          {"batsman": "SC Ganguly", "total_runs": 5, "extra_runs": 0}
        ]
      }




describe('Find the strike rate of a batsman for each season' , () =>{
    
    it('test case 2',() => {
        expect(prob7(sampleData)).toStrictEqual({
  '2008': { 'BB McCullum': 350, 'SC Ganguly': 100, 'RT Ponting': 250 },
  '2009': { 'BB McCullum': 180, 'SC Ganguly': 350, 'RT Ponting': 200 },
  '2010': { 'BB McCullum': 200, 'SC Ganguly': 375, 'RT Ponting': 200 },
  '2011': { 'BB McCullum': 187.5, 'SC Ganguly': 350, 'RT Ponting': 283.33 },
  '2012': { 'BB McCullum': 210, 'SC Ganguly': 262.5, 'RT Ponting': 257.14 },
  '2013': { 'BB McCullum': 245.45, 'SC Ganguly': 277.78, 'RT Ponting': 300 },
  '2014': { 'BB McCullum': 276.92, 'SC Ganguly': 290, 'RT Ponting': 300 },
  '2015': { 'BB McCullum': 271.43, 'SC Ganguly': 283.33, 'RT Ponting': 292.86 },
  '2016': { 'BB McCullum': 260, 'SC Ganguly': 285.71, 'RT Ponting': 312.5 },
  '2017': { 'BB McCullum': 256.25, 'SC Ganguly': 318.75, 'RT Ponting': 322.22 }
})
    })
})

//console.log(prob7(sampleData))