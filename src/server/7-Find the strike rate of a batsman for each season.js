function strikeRateEachBatsmanPerYear(yearWiseDeliveries) {
  tempResult = {};
  ans = {};
  for (let Year in yearWiseDeliveries) {
    for (let ball of yearWiseDeliveries[Year]) {
      if (tempResult[ball["batsman"]] === undefined) {
        tempResult[ball["batsman"]] = [
          1,
          parseInt(ball["total_runs"]) - parseInt(ball["extra_runs"]),
        ];
      } else {
        tempResult[ball["batsman"]][0]++;
        tempResult[ball["batsman"]][1] +=
          parseInt(ball["total_runs"]) - parseInt(ball["extra_runs"]);
      }
    }
    let econmmy = {};
    for (let year in tempResult) {
      econmmy[year] = parseFloat(
        ((tempResult[year][1] / tempResult[year][0]) * 100).toFixed(2)
      );
    }
    ans[Year] = econmmy;
  }

  //    }
  //console.log(ans[2016])
  return ans;
}
exports.prob7 = strikeRateEachBatsmanPerYear;
