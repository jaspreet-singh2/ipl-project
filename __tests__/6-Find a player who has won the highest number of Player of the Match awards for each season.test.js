const prob6= require("../src/server/6-Find a player who has won the highest number of Player of the Match awards for each season").prob6;

const sampleData =
{
  "2008": [
    {
      "player_of_match": "BB McCullum"
    },
    {
      "player_of_match": "MEK Hussey"
    },
    {
      "player_of_match": "MF Maharoof"
    },
    {
      "player_of_match": "MV Boucher"
    },
    {
      "player_of_match": "DJ Bravo"
    }
  ],
  "2009": [
    {
      "player_of_match": "YK Pathan"
    },
    {
      "player_of_match": "SR Watson"
    },
    {
      "player_of_match": "YK Pathan"
    },
    {
      "player_of_match": "BJ Hodge"
    },
    {
      "player_of_match": "MK Pandey"
    }
  ],
  "2010": [
    {
      "player_of_match": "S Anirudha"
    },
    {
      "player_of_match": "JH Kallis"
    },
    {
      "player_of_match": "YK Pathan"
    },
    {
      "player_of_match": "A Nehra"
    },
    {
      "player_of_match": "S Badrinath"
    }
  ],
  "2011": [
    {
      "player_of_match": "S Badrinath"
    },
    {
      "player_of_match": "CH Gayle"
    },
    {
      "player_of_match": "CH Gayle"
    },
    {
      "player_of_match": "MEK Hussey"
    },
    {
      "player_of_match": "M Vijay"
    }
  ],
  "2012": [
    {
      "player_of_match": "JD Unadkat"
    },
    {
      "player_of_match": "RG Sharma"
    },
    {
      "player_of_match": "AB de Villiers"
    },
    {
      "player_of_match": "AM Rahane"
    },
    {
      "player_of_match": "DA Miller"
    }
  ],
  "2013": [
    {
      "player_of_match": "SP Narine"
    },
    {
      "player_of_match": "CH Gayle"
    },
    {
      "player_of_match": "A Mishra"
    },
    {
      "player_of_match": "R Dravid"
    },
    {
      "player_of_match": "KA Pollard"
    }
  ],
  "2014": [
    {
      "player_of_match": "M Vijay"
    },
    {
      "player_of_match": "GJ Maxwell"
    },
    {
      "player_of_match": "RV Uthappa"
    },
    {
      "player_of_match": "RV Uthappa"
    },
    {
      "player_of_match": "AB de Villiers"
    }
  ],
  "2015": [
    {
      "player_of_match": "Harbhajan Singh"
    },
    {
      "player_of_match": "DA Warner"
    },
    {
      "player_of_match": "SE Marsh"
    },
    {
      "player_of_match": "A Nehra"
    },
    {
      "player_of_match": "DA Warner"
    }
  ],
  "2016": [
    {
      "player_of_match": "AM Rahane"
    },
    {
      "player_of_match": "KH Pandya"
    },
    {
      "player_of_match": "MS Dhoni"
    },
    {
      "player_of_match": "AB de Villiers"
    },
    {
      "player_of_match": "BCJ Cutting"
    }
  ],
  "2017": [
    {
      "player_of_match": "NM Coulter-Nile"
    },
    {
      "player_of_match": "JC Buttler"
    },
    {
      "player_of_match": "SP Narine"
    },
    {
      "player_of_match": "SV Samson"
    },
    {
      "player_of_match": "NM Coulter-Nile"
    }
  ]
}





describe('6-Find a player who has won the highest number of Player of the Match awards for each season' , () =>{
    
    it('test case 2',() => {
        expect(prob6(sampleData)).toStrictEqual({
          '2008': 'BB McCullum',
          '2009': 'YK Pathan',
          '2010': 'S Anirudha',
          '2011': 'CH Gayle',
          '2012': 'JD Unadkat',
          '2013': 'SP Narine',
          '2014': 'RV Uthappa',
          '2015': 'DA Warner',
          '2016': 'AM Rahane',
          '2017': 'NM Coulter-Nile'
        })
    })
})

//console.log(prob6(sampleData))