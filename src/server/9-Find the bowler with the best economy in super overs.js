function compare_economy(a, b) {
  return a[Object.keys(a)[0]] - b[Object.keys(b)[0]];
}

function bowlerWithBestEconomy(deliveries) {
  ballsAndtotalRuns = {};
  for (let ball of deliveries) {
    if (ball["is_super_over"] == 1) {
      if (ballsAndtotalRuns[ball["bowler"]] === undefined) {
        ballsAndtotalRuns[ball["bowler"]] = [1, parseInt(ball["total_runs"])];
      } else {
        ballsAndtotalRuns[ball["bowler"]][0]++;
        ballsAndtotalRuns[ball["bowler"]][1] += parseInt(ball["total_runs"]);
      }
    }
  }
  let ans = [];
  for (let bowler in ballsAndtotalRuns) {
    let econmmy = {};
    econmmy[bowler] = parseFloat(
      (
        ballsAndtotalRuns[bowler][1] /
        (ballsAndtotalRuns[bowler][0] / 6)
      ).toFixed(2)
    );
    ans.push(econmmy);
  }
  ans.sort(compare_economy);

  //console.log(ans);
  return ans;
}

exports.prob9 = bowlerWithBestEconomy;
