
const {prob9} = require("../src/server/9-Find the bowler with the best economy in super overs");

const sampleData =[
    {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "2",
        "is_super_over": "1"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "4",
        "is_super_over": "1"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "0",
        "is_super_over": "0"
      },
      {
        "bowler": "SR Watson",
        "total_runs": "4",
        "is_super_over": "1"
      },
      {
        "bowler": "SR Watson",
        "total_runs": "4",
        "is_super_over": "0"
      },
      {
        "bowler": "SR Watson",
        "total_runs": "0",
        "is_super_over": "1"
      },
      {
        "bowler": "SR Watson",
        "total_runs": "4",
        "is_super_over": "0"
      },
      {
        "bowler": "SR Watson",
        "total_runs": "4",
        "is_super_over": "0"
      },
      {
        "bowler": "SR Watson",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "0",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "TM Head",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "TM Head",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "TM Head",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "TM Head",
        "total_runs": "6",
        "is_super_over": "1"
      },
      {
        "bowler": "TM Head",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "TM Head",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "2",
        "is_super_over": "0"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "2",
        "is_super_over": "0"
      },
      {
        "bowler": "YS Chahal",
        "total_runs": "2",
        "is_super_over": "0"
      },
      {
        "bowler": "A Choudhary",
        "total_runs": "0",
        "is_super_over": "1"
      },
      {
        "bowler": "A Choudhary",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "A Choudhary",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "A Choudhary",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "A Choudhary",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "A Choudhary",
        "total_runs": "0",
        "is_super_over": "0"
      },
      {
        "bowler": "STR Binny",
        "total_runs": "1",
        "is_super_over": "0"
      },
      {
        "bowler": "STR Binny",
        "total_runs": "4",
        "is_super_over": "0"
      },
      {
        "bowler": "STR Binny",
        "total_runs": "0",
        "is_super_over": "0"
      },
      {
        "bowler": "STR Binny",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "STR Binny",
        "total_runs": "2",
        "is_super_over": "0"
      },
      {
        "bowler": "STR Binny",
        "total_runs": "2",
        "is_super_over": "0"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "1",
        "is_super_over": "1"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "0",
        "is_super_over": "0"
      },
      {
        "bowler": "S Aravind",
        "total_runs": "1",
        "is_super_over": "1"
      },
]


describe('9-Find the bowler with the best economy in super overs' , () =>{
    
    it('test case 2',() => {
        expect(prob9(sampleData)).toStrictEqual([
  { 'A Choudhary': 4 },
  { 'YS Chahal': 5 },
  { 'STR Binny': 6 },
  { 'S Aravind': 10 },
  { 'SR Watson': 10 },
  { 'TM Head': 16 }
])
    })
})

//console.log(prob9(sampleData))