const csv = require("csv-parser");
const fs = require("fs");
const filter = require("./filter-data").filter;
let results = [];
let results2 = [];

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  .on("data", (data) => results.push(data))
  .on("end", () => {
    let matches = JSON.parse(JSON.stringify(results));

    fs.createReadStream("../data/deliveries.csv")
      .pipe(csv())
      .on("data", (data) => results2.push(data))
      .on("end", () => {
        let deliveries = JSON.parse(JSON.stringify(results2));
        filter(matches, deliveries);
      });
  });
