function extraRunsConceivedPerTeam(matchesin2016) {
  ans = {};
  for (let match of matchesin2016) {
    if (ans[match["bowling_team"]] === undefined) {
      ans[match["bowling_team"]] = parseInt(match["extra_runs"]);
    } else {
      ans[match["bowling_team"]] += parseInt(match["extra_runs"]);
    }
  }

  //console.log(ans)

  return ans;
}
exports.prob3 = extraRunsConceivedPerTeam;
